## Ipsumlab Laravel Security Package

What's included
* Middlewares CORS, XSS
* Validation Rules

### Middlewares
in `app/Http/Kernel.php` add

```
    protected $routeMiddleware = [
        ...
        'xss' =>  Ipsumlab\Security\Middleware\XSS::class,
        'cors' =>  Ipsumlab\Security\Middleware\CORS::class,
    ];
```

in you `web.php` you can use as follow

```
    Route::group([ 'middleware' => ['auth', 'xss', 'cors']], function () {
    
        // Your routes
         
    });
```


### Rules

* #### NoPhpExtension

    Avoid malicious file extensions that pass mime validation like image files with some php script injected. 

    ***The attribute must be a file.***

* #### StrongPassword

    The rule include a regexp to test a simple strong password

#### Usage
``` 
use Ipsumlab\Security\Rules\NoPhpExtension;
use Ipsumlab\Security\Rules\StrongPassword;

...

$validatedData = $request->validate([
  //... other rules 
  'myfile'  => ['mimes:png', new NoPhpExtension()],
  'password' => ['required', 'confirmed', new StrongPassword()],
]);
 ```

#### Translations
in your `resources/lang/*/validation.php` put
```
'strong_password' => 'The :attribute must be at least 8 characters including a number, a symbol, a lowercase character, and an uppercase character',
'php_extension' => 'The file extension is not allowed'
```

