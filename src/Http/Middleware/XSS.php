<?php

namespace Ipsumlab\Security\Http\Middleware;

use Closure;

class XSS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array(strtolower($request->method()), ['put', 'post'])) {
            return $next($request);
        }

        $input = $request->all();

        array_walk_recursive($input, function(&$input) {

            if($this->stripUnsafe($input) === 1){
                abort(400, 'XSS detected');
            }
        });

        $request->merge($input);

        return $next($request);
    }


    private function stripUnsafe($input, $img = false)
    {

        $unsafe = [
            //'/<iframe(.*?)<\/iframe>/is',
            '<title(.*?)<\/title>',
            '<pre(.*?)<\/pre>',
            '<frame(.*?)<\/frame>',
            '<frameset(.*?)<\/frameset>',
            '<object(.*?)<\/object>',
            '<script(.*?).*>',
            '<embed(.*?)<\/embed>',
            '<applet(.*?)<\/applet>',
            '<meta(.*?)>',
            '<!doctype(.*?)>',
            '<link(.*?)>',
            '<body(.*?)>',
            '<\/body>',
            '<head(.*?)>',
            '<\/head>',
            'on[a-z]*="(.*?)"',
            "on[a-z]*='(.*?)",
            'javascript:(.*?)',
            '<html(.*?)>',
            '<\/html>',
        ];

        if ($img == true) {
            $unsafe[] = '/<img(.*?)>/is';
        }

        if(is_null($input)){
            return $input;
        }
        return preg_match("/".$this->array_implode('|', $unsafe). "/is", $input);
    }
    private function array_implode($separator, $array){
        return PHP_VERSION_ID >= 70400 ? implode($separator, $array) : implode($array, $separator);
    }
}
