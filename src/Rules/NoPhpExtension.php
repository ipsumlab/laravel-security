<?php

namespace Ipsumlab\Security\Rules;

use Illuminate\Contracts\Validation\Rule;

class NoPhpExtension implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^(php|php.*|phar|phtml)$/i', $value->getClientOriginalExtension()) == false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.php_extension');
    }
}
